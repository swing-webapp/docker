#!/bin/bash


TOKEN=`cat /dev/urandom | tr -dc '(\&\_a-zA-Z0-9\^\*\@' | fold -w ${1:-64} | head -n 1`
echo "TOKEN=$TOKEN" > tokenapi.env
