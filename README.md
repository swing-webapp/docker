Docker Eawag Survey
======

This repository created the docker containers needed to run the Eawag survey web application

### Create a new docker instance

To create a new docker instance use (for production version) :

1. ```git clone https://gitlab.switch.ch/swing-webapp/docker.git```
1. ```cd docker```
1. Generate token for api (only once) :  ```./makeenv.sh```   
1. ```(sudo) docker-compose up```

By default the webserver run on port ```9080```, can be changed in the ```docker-compose.yaml``` file.

### Other commands

* To stop : ```(sudo) docker-compose stop```
* To stop and erase data : ```(sudo) docker-compose down```
* To run in the background ```(sudo) docker-compose up -d ```
* To rebuild (and replace) when running in the background ```(sudo) docker-compose up -d --build```

## Production instance

The gitlab address of development code is [https://gitlab.switch.ch/swing-webapp](https://gitlab.switch.ch/swing-webapp)

The URL docker git is ```https://gitlab.switch.ch/swing-webapp/docker.git```


## File structure

    ./docker-compose.yaml : The main docker file that use docker-compose to combine all the containers together
    ./docker-sshkey.pub : the public key shared with the gitlab instance
    ./nginx-app.conf : the config file of the nginx webserver
    ./LICENSE : MIT license
    ./Dockerfile-api : The dockerfile to create the API docker
    ./README.md :
    ./docker-sshkey : the private key shared with the gitlab instance
    ./Dockerfile-app : The dockerfile to create the webserver docker
